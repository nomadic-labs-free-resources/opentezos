---
id: wrapped-assets
title: Wrapped Assets
author: Aymeric Bethencourt
---

## Wrapped XTZ
Let's consider the following facts:

- Tez (XTZ) is the native currency built on the Tezos blockchain.

- When a dApp (decentralized application) is built from the Tezos Blockchain, it usually either implements its own form of token or needs to work with existing tokens, both are based on [FA standards](/defi/token-standards).

- The _FA1.2_ and _FA2_ standards define how tokens are transferred and how to keep a consistent record of those token transfers in the Tezos network. FA standards were developed after the release of the XTZ.

Now here is the issue: **XTZ doesn't conform to its own FA standards**.

Indeed, XTZ is the proto-token of the Tezos Blockchain, i.e. it was built before the FA standards existed. This makes XTZ not compliant with the FA standards used by most Dapps, e.g. DEXs, NFT marketplaces, etc.

![](non-compliant.svg)
<small className="figure">FIGURE 1: XTZ can't interact with FA tokens</small>

One solution consists in _wrapping_ XTZ into an FA-compliant token. Wrapping XTZ allows you to trade them directly with alt tokens on certain decentralized platforms. Because decentralized platforms running on Tezos use smart contracts to facilitate trades, directly between users, every user needs to have the same standardized format for the tokens they trade. This ensures tokens don't get lost.

![](wrap.svg)
<small className="figure">FIGURE 2: Wrapping and unwrapping XTZ</small>

When you "wrap" XTZ, you aren't really wrapping so much as trading XTZ for an equal token called wXTZ via a smart contract. If you want to get plain XTZ back, you need to "unwrap" it, i.e. trade it back for XTZ.

In practice, when wrapping, your XTZ are stored in a smart contract, and an equal amount of wXTZ is minted by the contract and transferred to you. When unwrapping, your wXTZ are burned (a.k.a. destroyed), and some XTZ are released and sent back to you.

![](compliant.svg)
<small className="figure">FIGURE 3: wXTZ can interact with other FA tokens</small>

## Other wrapped assets

One may want to interact with other assets, such as Ethereum or Bitcoin from a Tezos Smart contract. A wrapped asset can bridge an asset from a different native public blockchain network to the one in which it is wrapping itself.

[StableTech](https://stable.tech/) has created [Wrapped ETH (ETHtz)](https://decrypt.co/51860/wrapped-eth-comes-to-tezos-as-it-takes-on-ethereum-defi-market) which is a FA1.2 token with a price pegged to ETH. ETHtz can be used on Tezos for exchanges or DeFi service while taking advantage of Tezos's much lower fees than Ethereum.

[Wrapped Bitcoin (tzBTC)](https://tzbtc.io/) is another wrapped asset on Tezos pegged to BTC. tzBTC is also implemented using the FA1.2 asset standard on Tezos.

This way, one can use the consensus mechanism and the specific infrastructure of Tezos to use assets or information stored with both Tezos and Ethereum.

## Conclusion

Wrapped assets not only improve the functionality and usability of the asset it wraps, it also opens up a wide array of higher-level financial services that wouldn't be available otherwise.

On Tezos, we've seen the addition of [Wrapped Bitcoin (tzBTC)](https://tzbtc.io/), [Wrapped ETH (ETHtz)](https://decrypt.co/51860/wrapped-eth-comes-to-tezos-as-it-takes-on-ethereum-defi-market) and the addition of ERC-20 from [Plenty Defi](https://analytics.plentydefi.com/) wrapped assets (among which, **WBTC**, **USDC**, **WETH**, **BUSD**, **DAI**, **USDT**, **LINK**, **MATIC**, **agEUR**).

## References

[1] <https://medium.com/stakerdao/the-wrapped-tezos-wxtz-beta-guide-6917fa70116e>

[2] <https://tzbtc.io/>
