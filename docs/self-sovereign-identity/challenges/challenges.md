---
id: challenges
title: Issues surrounding decentralized identity
authors: Daniel Nomadic Labs
---

An identity-management platform is a complex and sensitive tool. Its impact on the life of its users is highly significant and if poorly managed, the consequences can be serious. The risks involved should therefore be understood at the earliest opportunity, so that all the necessary protections can be put in place. These risks may vary depending on where the identity system is implemented. The issues differ according to the GDP and level of access to the Internet and digital technologies of the country in which such a solution is established.

The following list of risks was identified by the World Bank Group 5.

## Risk of exclusion

When a new ID system is put in place, one of the key elements is the strengthening of identification requirements for the population concerned. It is important to keep in mind that a population that was identified by unofficial or informal documents in the old system risks being excluded from the new one due to being unable to meet its basic requirements (access to a reliable Internet connection or ownership of smart objects).

All research toward the development of a decentralized identity management system must include an analysis of the risks of exclusion in order to ensure that the majority of the population in question can benefit from it. This is particularly important for high-risk groups such as remote or isolated residents, minorities, disabled people, people with poor connections, and those unable to read or write.

It is therefore essential to take this sort of risk into account when devising solutions that could extend identification requirements.

## Invasion of privacy and security breaches

The risks of invasion of privacy are not specific to decentralized identity solutions; they are shared by any application that collects, stores, and uses personal data. Other dangers associated with the collection and use of personal data include the theft or misuse of data, fraud, identity theft, and discrimination.

Identity-management systems can be lucrative targets for hacking groups. Such systems must therefore be closely supervised and heavily regulated to ensure maximum security for users and their data. A “privacy-and-security-by-design” approach must be taken from the very beginning to ensure all the necessary protections are put in place.

An evaluation of privacy and security risks must be included in the planning process (for example, an analysis of the impact on data protection, penetration tests, and cybersecurity audits) and must continue throughout every stage of the implementation of the identification system.

## Audit

During audits, the decentralized identity solution must be capable of providing information on its users without compromising the basic principles of its system, namely decentralization, security, and anonymity. If the solution is used on a countrywide scale, the regulators must also be able to verify the actions of certain entities under certain conditions.