---
id: tezos-performances
title: Tezos performances
authors: Nomadic Labs
---

:::info
In this article, all performance statistics are given theoretically. Only layer 1 performance is evaluated, this article doesn't take into account Smart Rollups. All the data are estimates and may change depending on the used contract. If you wish to have access to a more empirical approach, you can read this [blog post](https://research-development.nomadic-labs.com/tps-evaluation.html).
:::

## Estimate Tezos performances

Below are the constants that matter to evaluate the maximum transfers per second:

* the gas limit of a block is **[:HARD_GAS_LIMIT_PER_BLOCK:] gas units**,

* the gas limit of an operation is **[:HARD_GAS_LIMIT_PER_OPERATION:] gas units[^1]**,

* the block time is **[:MINIMAL_BLOCK_DELAY:] seconds**,

* the maximum bytes size of an operation is **~33kB**,

* the maximum bytes size of a block is **~256kB**.

The gas limit of a block and the size of a block are the two factors that limit the number of operations that can be performed in a block.

[^1]: gas is accounted by an arbitrary unit so that one unit of gas represents one nanosecond of computation performed by the machines used during the benchmark.

### Tez transfer

A single transfer of tez performed by a tz1 account is using **169 gas units** (the different key schemes have different gas cost). On the table below, you can also notice that batching transactions into a single operation saves up a little gas compared to perform them separately. The limit size of an operation (33 kB) is reached with a batch of 583 transfers.

| Batch size   | Gas consumed  |
| :----------- | :--------|
| 1   | 169    |
| 10  | 1086   |  
| 100 | 10257  |
| 583 | 59472  |

In the end, for standard tez transfers, the chain can process approximately **~300 transfers per second**.

### FA1.2 transfer

A single transfer of a FA1.2 token is using **1784 gas units**.

With the same logic as for the xtz transfer, we got a result of theoretically **~100 FA1.2 transfers per second**:

### FA2 transfer

One interesting feature of the FA2 standard is the **transfer list** parameter that allows to directly pass to the contract a list of transfers to perform. Unlike batching, this will reduce the gas consumption significantly.

Here is a summary table of the gas used depending on the size of the transfer list:

|  Transfer list size  |  Gas consumed      |
| :--------------- |:--------|
| 1   |  1785
| 10  |  6084   |  
| 100 |  51144  |
| 300 |  167394 |  
| 650 |  380620 |

650 FA2 transfers can fit in a list before reaching the maximum size on an operation while consuming 380,620 gas units which is under the gas limit for an operation. In the end, the results are **~300 FA2 transfers per second**.
